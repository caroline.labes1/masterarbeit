import { Component, OnInit } from '@angular/core';
import { Topic } from '../models/topic';
import { ForumService } from '../services/forum.service';
import { UserService } from '../services/user.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Post } from '../models/post';

@Component({
  selector: 'app-new-topic-form',
  templateUrl: './new-topic-form.component.html',
  styleUrls: ['./new-topic-form.component.sass']
})
export class NewTopicFormComponent implements OnInit {
  topic: Topic = {title: undefined, author: undefined, timestamp: undefined, category: undefined,posts: [] };
  post: Post = {author: undefined, created: undefined, lastUpdate: undefined, content: undefined};
  category = ['Allgemein','Tagesbetreuung','Ernährung','Gesundheitliches','Technologie&Forschung'];
  public Editor = ClassicEditor;
  rte= '';
  selected: string;

  constructor(private forumService: ForumService, private userservice: UserService) { }

  ngOnInit(): void {
  }

  saveTopic(topic:Topic) {
    this.topic.author = this.userservice.getUserName();
    this.topic.timestamp = new Date();
    this.topic.category = this.selected;
    console.log(this.selected);
    this.post.author = this.topic.author;
    this.post.created = new Date();
    this.post.lastUpdate = new Date();
    this.post.content = this.rte;
    this.topic.posts.push(this.post);
    this.forumService.createTopic(topic);
  }

}
