import { Message } from '../models/message';

export interface Chat {
    messages: Message[];
}