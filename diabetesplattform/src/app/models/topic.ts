import { Post } from '../models/post';

export interface Topic {
    title: string;
    timestamp: Date;
    author: string;
    posts: Post[];
    category: string;

}