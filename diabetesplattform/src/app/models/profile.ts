import { Chat } from '../models/chat';

export interface Profile {
    name: string;
    relationship: string;
    job: string;
    hobbies: string;
    state: string;
    chats: Chat[];
  }