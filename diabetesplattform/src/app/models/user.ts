export interface User {
    id: string;
    username: string;
    email: string;
    profileUrl: string;
    chats: []
  }