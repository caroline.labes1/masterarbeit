export interface Post {
    content: string;
    created: Date;
    lastUpdate: Date;
    author: string;
    answers?: Post[];
}