import { Component, OnInit } from '@angular/core';
import { Topic } from '../models/topic';
import { ForumService } from '../services/forum.service';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';

export interface TopicId extends Topic { id: string; }

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.sass']
})
export class ForumComponent implements OnInit {

  topics : TopicId[];
  topic1: TopicId[] = [];
  topic2: TopicId[] = [];
  topic3: TopicId[] =[];
  topic4: TopicId[] = [];
  topic5: TopicId[] = [];
  displayedColumns: string[] = ['title', 'author', 'timestamp', 'answers'];

   constructor(private forumService: ForumService, public userservice: UserService) {
   }

  ngOnInit() {
    this.getAllTopics();
    console.log("Topics: "+this.topics);
  }

  getAllTopics() {
    this.forumService.getAllTopics()
    .subscribe(res => this.topics = res
      /*res.forEach(data =>{
        switch(data.category){
          case 'Allgemein': {this.topic1.push(data);
        }
        case 'Tagesbetreuung': {this.topic2.push(data);
        }
        case 'Ernährung':{ this.topic3.push(data);

        }
        case 'Gesundheitliches': {this.topic4.push(data)}
        
        case 'Technologien&Forschung': {this.topic5.push(data)

        }
      }
    }

      )*/
      );
    console.log(this.topic3);
  }
  deleteTopic(topicid: string){
    this.forumService.deleteTopic(topicid);
  }

}
