import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, DialogLogin, DialogReg } from './app.component';
import { DialogNewChat } from './privatemessages/privatemessages.component';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import 'firebase/storage';
import { AngularFireAuthGuard, redirectUnauthorizedTo, AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { environment } from '../environments/environment';

//Components
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UsersDashboardComponent } from './users-dashboard/users-dashboard.component';
import { ForumComponent } from './forum/forum.component';
import { NewTopicFormComponent } from './new-topic-form/new-topic-form.component';
import { ForumDetailComponent } from './forum-detail/forum-detail.component';
import { ProfileComponent } from './profile/profile.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog'; 
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatInputModule} from '@angular/material/input'; 
import {MatIconModule, MatIcon} from '@angular/material/icon'; 
import {MatTabsModule} from '@angular/material/tabs'; 
import {MatListModule} from '@angular/material/list'; 
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import {MatTableModule} from '@angular/material/table'; 
import {MatSidenavModule} from '@angular/material/sidenav'; 
import {MatTreeModule} from '@angular/material/tree'; 
import {MatCardModule} from '@angular/material/card'; 
import {MatButtonToggleModule} from '@angular/material/button-toggle'; 
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu'; 

// flex layout
import { FlexLayoutModule } from '@angular/flex-layout';

// Richtext Editor
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { ImageCropperModule } from 'ngx-image-cropper';
import { PrivatemessagesComponent } from './privatemessages/privatemessages.component';


const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: '', component: LandingpageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'dashboard', component: UsersDashboardComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'forum', component: ForumComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'new-topic', component: NewTopicFormComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'verify-email', component: VerifyEmailComponent },
  { path: 'forum/:id', component:ForumDetailComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'profile', component:ProfileComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'chat/:id', component:PrivatemessagesComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  { path: 'chat', component:PrivatemessagesComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }}

];
@NgModule({
  declarations: [
    AppComponent,
    LandingpageComponent,
    LoginComponent,
    RegistrationComponent,
    UsersDashboardComponent,
    ForumComponent,
    DialogLogin,
    DialogReg,
    NewTopicFormComponent,
    VerifyEmailComponent,
    ForumDetailComponent,
    ProfileComponent,
    PrivatemessagesComponent,
    DialogNewChat
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatTabsModule,
    MatListModule,
    MatDatepickerModule,
    MatTableModule,
    MatSidenavModule,
    MatTreeModule,
    CKEditorModule,
    MatCardModule,
    ImageCropperModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatMenuModule
  ],
  exports: [RouterModule],
  providers: [AngularFirestore, AngularFireAuth, AngularFireAuthGuard],
  entryComponents: [DialogLogin, DialogReg, DialogNewChat],
  bootstrap: [AppComponent]
})
export class AppModule { }
