import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UserService } from 'src/app/services/user.service';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Observable } from 'rxjs';

// Login Dialog Data
export interface DialogDataLogin {
  email: string;
  password: string;
}
// Reg Dialog Data
export interface DialogDataReg {
  email: string;
  password: string;
  name: string;
}

// Tree Nav
interface MenuNode {
  name: string;
  url?: string;
  children?: MenuNode[];
}

const TREE_DATA: MenuNode[] = [
  {
    name: 'Wissenswertes',
    url: '',
    children: [
      {name: 'Erkrankung',
      url: '',
      children: [
        {name: 'Was ist Diabetes?',
        url: ''},
        {name: 'Typ-1',
        url: ''},
        {name: 'Typ-2',
        url: ''},
        {name: 'Sonderformen',
        url: ''}
      ]},
      {name: 'Therapien',
      url: ''},
      {name: 'Technologien & Forschung',
      url: ''},
      {name: 'Ernährung',
      url: ''},
      {name: 'Tipps & Tricks',
      url: ''},
      {name: 'Kinder',
      url: ''}
    ]
  }, 
  {name: 'Dashboard',
  url: '/dashboard'
},
  {
    name: 'Forum',
    url: '/forum',
    children: [
      {
        name: 'Allgemein',
        url: ''
      }, {
        name: 'Freizeit',
        url: ''
      },
      {name: 'Ernährung',
      url: ''},
      {name: 'Technologien & Forschung',
      url: '',},
      {name: 'Rechtliches & Pflege',
      url: '',},
      {name: 'Kinder',
      url: ''}
    ]
  },
  {name: 'Profil',
  url: '/profile'},
  {name: 'Einstellungen',
  url: ''}
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

// Home Component
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  

  constructor(public dialog: MatDialog, public userservice: UserService){
    this.dataSource.data = TREE_DATA;
  }
// Login Form
 
  title = 'UnserDiabetes';
  showFiller = false;
  email: string;
  password: string;
  name: string;
  hide = true;

  loggedin: Observable<boolean>;

  // check user state
  ngOnInit() {
  }

  logout() {
    this.userservice.logoutUser();
  }


  // TreeNav
  private _transformer = (node: MenuNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      url: node.url
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


  // Login Form
  openLoginDialog(): void {
    const dialogLogin = this.dialog.open(DialogLogin, {
      width: '500px',   
      data: {email: this.email, password: this.password}
    });

    dialogLogin.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.userservice.loginUser(result.email, result.password);
      console.log('User LoggedIn');
    });
  }

  // Registration Form
  openRegDialog(): void {
    const dialogReg = this.dialog.open(DialogReg, {
      width: '500px',
      data: {email: this.email, password: this.password, name: this.name}
    });

    dialogReg.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.userservice.createUser(result.email, result.password, result.name);
      console.log("User created");
    });
  }
  
}

// Email Error Check
export class EmailErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || isSubmitted));
  }
}

// Required Error Checker (Benutzername)
export class RequiredErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || isSubmitted));
  }
}

// Login-Form

@Component({
  selector: 'dialog-login',
  templateUrl: 'dialog-login.html',
})
export class DialogLogin {

  hide = true;

  constructor(
    public dialogLogin: MatDialogRef<DialogLogin>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataLogin) {}

  onNoClick(): void {
    this.dialogLogin.close();
  }

}



// Reg-Form

@Component({
  selector: 'dialog-registration',
  templateUrl: 'dialog-registration.html',
})
export class DialogReg {

  hide = true;

  constructor(
    public dialogReg: MatDialogRef<DialogReg>, @Inject(MAT_DIALOG_DATA) public data: DialogDataReg) {
    }   

  onNoClick(): void {
    this.dialogReg.close();
  }


}
