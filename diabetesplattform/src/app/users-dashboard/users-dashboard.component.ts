import { Component, OnInit } from '@angular/core';
import { UserService} from '../services/user.service';
import { ProfileService} from '../services/profile.service';
import { Profile } from '../models/profile';

@Component({
  selector: 'app-users-dashboard',
  templateUrl: './users-dashboard.component.html',
  styleUrls: ['./users-dashboard.component.sass']
})
export class UsersDashboardComponent implements OnInit {
username: string;
//users: Profile[];

  constructor(private userservice: UserService, private profileservice: ProfileService) { }

  ngOnInit() {
    this.username = this.getUserName();
  //  this.getUsernames();
   // console.log(this.users);

  }

  getUserName() {
    return this.userservice.getUserName();
  }

 /* getUsernames(){
  this.profileservice.getAllUsers()
    .subscribe(us => this.users = us);
  }*/

}
