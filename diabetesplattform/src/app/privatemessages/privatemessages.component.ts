import { Component, Inject, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Chat } from '../models/chat';
import { Message } from '../models/message';
import { ChatService } from '../services/chat.service';
import {ProfileService} from '../services/profile.service';
import { Profile } from '../models/profile';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

//export interface ProfileId extends Profile { id: string; }
export interface DialogDataChatlist {
  id: string;
  name: string;
}


@Component({
  selector: 'app-privatemessages',
  templateUrl: './privatemessages.component.html',
  styleUrls: ['./privatemessages.component.sass']
})
export class PrivatemessagesComponent implements OnInit {

  users: { id: string, name: string }[] ;
  chat: Chat;
  currusername: string;
  messages: Message[] = [];
  messa: string;
 // currProfile: Profile;

  constructor(private profileservice: ProfileService, private userservice: UserService, public dialog: MatDialog, public router: Router, private chatservice: ChatService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllUsers();
    console.log(this.users);
    this.currusername = this.getUserName();
    if(this.route.snapshot.paramMap.get('id')){
      this.loadChat(this.route.snapshot.paramMap.get('id'));
    }
  }

  getUserName() {
    return this.userservice.getUserName();
  }

  getAllUsers(){
    //new Chat list
    this.profileservice.getAllUsers()
      .subscribe(us => this.users = us)       

  }

  createNewChat(usser: DialogDataChatlist){
    //console.log("iD: "+usser.id);
    this.chatservice.createChat(usser.id, usser.name);
    //this.router.navigate(['/chat', id: chatid]);
  }

  loadChat(chatid: string) {
    this.chatservice.getChat(chatid)
    .subscribe(mes => this.chat = mes);
  }

  sendMessage(text: string){
    const mes = {sender: this.getUserName(), text: text, time: new Date()};
    //this.messages.push(mes);
    if(!this.chat.messages){
      this.chat.messages = [];
    }
    this.chat.messages.push(mes);
    let chatid = this.route.snapshot.paramMap.get('id');
    // push message to messages
    console.log("bevor service");
    this.chatservice.sendMessage(chatid, this.chat);
    this.messa='';
  }

  // ******************* Dialog New Chat ***************************************
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogNewChat, {
      width: '400px',
     // height: '600px',
      data: this.users
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.createNewChat(result);
    });
  }

  
}

@Component({
  selector: 'dialog-new-chat',
  templateUrl: 'dialog-new-chat.html',
})
export class DialogNewChat {
  constructor(public dialogRef: MatDialogRef<DialogNewChat>, @Inject(MAT_DIALOG_DATA) public data: DialogDataChatlist) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}