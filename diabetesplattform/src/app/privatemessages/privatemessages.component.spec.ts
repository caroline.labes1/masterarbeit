import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivatemessagesComponent } from './privatemessages.component';

describe('PrivatemessagesComponent', () => {
  let component: PrivatemessagesComponent;
  let fixture: ComponentFixture<PrivatemessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivatemessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivatemessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
