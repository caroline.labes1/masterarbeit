import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ForumService, TopicId } from '../services/forum.service';
import { switchMap } from 'rxjs/operators';
import { Topic } from '../models/topic';
import { Observable } from 'rxjs';
import { Post } from '../models/post';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-forum-detail',
  templateUrl: './forum-detail.component.html',
  styleUrls: ['./forum-detail.component.sass']
})
export class ForumDetailComponent implements OnInit {

  topic: TopicId;
  topi: Topic;
  post: Post;
  id: string;
  postrte: string = '';
  ansrte = [];
  public Editor = ClassicEditor;
  hideme = [];

  

  constructor(private route: ActivatedRoute, private router: Router, private forumservice: ForumService, private userservice: UserService) {

   }


  ngOnInit() {
    this.getTopic();
    for(let i in this.topic.posts){
      this.hideme[i] = true;
      this.ansrte[i] = '';
    }
  }

  getTopic(){
    this.id = this.route.snapshot.paramMap.get('id');
  this.forumservice.getTopic(this.id)
    .subscribe(top => this.topic = top);
        
  }

  createPost() {
    this.post = {author: this.userservice.getUserName(), created: new Date(), lastUpdate: new Date(), content: this.postrte};
    this.topic.posts.push(this.post);
    
    this.topi = {author: this.topic.author, title: this.topic.title, timestamp: this.topic.timestamp, category: this.topic.category, posts: this.topic.posts}

    this.forumservice.updateTopic(this.topi, this.id);

    this.postrte = '';
  }

  commentPost(i:number){
    this.post = {author: this.userservice.getUserName(), created: new Date(), lastUpdate: new Date(), content: this.ansrte[i]};

    this.topic.posts[i] = {author: this.topic.posts[i].author, content: this.topic.posts[i].content, created: this.topic.posts[i].created, lastUpdate: this.topic.posts[i].lastUpdate, answers: this.topic.posts[i].answers};
    if(!this.topic.posts[i].answers){
      this.topic.posts[i].answers = [];
    }
      this.topic.posts[i].answers.push(this.post);
    
    
    this.topi = {author: this.topic.author, title: this.topic.title, timestamp: this.topic.timestamp, category: this.topic.category, posts: this.topic.posts}
    this.forumservice.updateTopic(this.topi, this.id);
    this.ansrte[i] = '';
    this.hideme[i]=true;
  }

 

}
