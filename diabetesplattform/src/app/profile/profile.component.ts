import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { Profile } from '../models/profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  username: string;
  user: firebase.User;
  profileData: Profile;

  constructor(private userservice: UserService) { }

  ngOnInit() {
    this.userservice.getUserInfo().subscribe(
      res =>this.user = res
    )
    this.username = this.userservice.getUserName();
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';
  image: File;
  filename: string = '';
  datatype: string = '';
  
  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
      // get image name + datatype
      if(event.target.files.length > 0) {
        var split = event.target.files[0].name.split('.');
        this.filename=split[0];
        if(split[1]=='jpg'){
            this.datatype='jpeg';
        }else {
            this.datatype=split[1];
        }
        console.log('filename: '+ this.filename);
        console.log('datatype: '+ this.datatype);
        
    }
  }

  imageCropped(event: ImageCroppedEvent) {
      console.log("cropped");
      this.croppedImage = event.base64;
      console.log(event);
  }

  imageLoaded() {
      // show cropper
      console.log("image loaded");
  }

  cropperReady() {
      // cropper ready
      console.log("cropper ready");
  }

  loadImageFailed() {
      // show message
      console.log("fail");
  }

  getImage(aFile) {
    this.image = aFile;
  }

  savePic(){
    
    const blob = this.dataURItoBlob(this.croppedImage);
    
    this.userservice.uploadImage(blob).subscribe(
      url => this.user.photoURL = url
    );
  }
  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(',')[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/'+this.datatype
    });

    }

    saveProfileData() {
      
    }

}
