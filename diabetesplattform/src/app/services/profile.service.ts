import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Profile } from '../models/profile';

export interface ProfileId extends Profile { id: string; }

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private userCollection: AngularFirestoreCollection = this.firestore.collection('User');

  constructor(private http: HttpClient, private firestore: AngularFirestore, public auth: AngularFireAuth) { }

  getAllUsers(): Observable<{id: string, name: any}[]>{
    return this.userCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const name = a.payload.doc.data().name;
        const id = a.payload.doc.id;

        return {id, name} ;
      }))
    )
  }

  getCurrUserProfile(){
    const uid = this.auth.auth.currentUser.uid;
    return this.userCollection.doc(uid).valueChanges();
  }
}
