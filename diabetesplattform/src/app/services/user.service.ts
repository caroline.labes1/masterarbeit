import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from "@angular/router";
import { Observable } from 'rxjs';
import { state } from '@angular/animations';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import 'firebase/storage';
import { finalize } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: Observable<firebase.User>;


  constructor(public auth: AngularFireAuth, public router: Router, private storage: AngularFireStorage, private firestore: AngularFirestore) {
    this.user = auth.authState;
   }

  createUser(email: string, password:string, name: string){
    return this.auth.auth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      this.sendVerificationMail(); // Sending email verification notification, when new user registers
      const user = this.auth.auth.currentUser;
      if (user){
        user.updateProfile({
          displayName: name
      });
      this.firestore.collection('User').doc(user.uid).set({
        name: name,
        hobbies: '',
        job: '',
        relationship: '',
        state: '',
        chats: []
      }
    )

    }
    }).catch((error) => {
      window.alert(error.message)
    })
   
    /*.catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode == 'auth/weak-password') {
        alert('The password is too weak.');
      } else {
        alert(errorMessage);
      }
    console.log(error);
    });*/
  }

  sendVerificationMail() {
    return this.auth.auth.currentUser.sendEmailVerification()
    .then(() => {
      this.router.navigate(['/verify-email']);
    })
  }

  loginUser(email: string, password: string){
    return this.auth.auth.signInWithEmailAndPassword(email, password)
    .then((result) => {
      if (this.auth.auth.currentUser.emailVerified === true){
        this.router.navigate(['/dashboard']);
        console.log('Username: '+this.auth.auth.currentUser.displayName);
      } else {
        this.router.navigate(['/verify-email']);
      }
    })
   /* .catch(function(error) {
  // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
    });*/
  }

  logoutUser() {
    this.auth.auth.signOut();
  }

 
  getUserName () {
    return this.auth.auth.currentUser.displayName;
  }



  downloadurl: Observable<string>;
  uploadImage(file: any): Observable<string> {
   const filePath = 'gs://diabetesplattform.appspot.com/users-profile-images/';
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
   // this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() =>  this.downloadurl =  fileRef.getDownloadURL() )
     )
    .subscribe()
    return this.downloadurl;
  }


  

 getUserInfo(){
   return this.user;
 }
 /* updateUserProfile(name: string, photourl: string){


    this.user.updateProfile({
      displayName: "Jane Q. User",
      photoURL: "https://example.com/jane-q-user/profile.jpg"
    }).then(function() {
      // Profile updated successfully!
      // "Jane Q. User"
      var displayName = user.displayName;
      // "https://example.com/jane-q-user/profile.jpg"
      var photoURL = user.photoURL;
    }, function(error) {
      // An error happened.
    });
  }
  updateUserDBProfile(pro: Profile, id: string){

  }*/

  

  deleteUser(){

  }


}
