import { Injectable } from '@angular/core';
import { Topic } from '../models/topic';
import { Post } from '../models/post';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface TopicId extends Topic { id: string; }

@Injectable({
  providedIn: 'root'
})

export class ForumService {
  private topicCollection: AngularFirestoreCollection<Topic> = this.firestore.collection<Topic>('Topic');

  constructor(private http: HttpClient, private firestore: AngularFirestore, public router: Router) { }

  getAllTopics(): Observable<TopicId[]>{
    return this.topicCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Topic;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    )
  }

  getTopic(id: string): Observable<TopicId> {
    const topicDocuments = this.firestore.doc<TopicId>('Topic/' + id);
    return topicDocuments.snapshotChanges().pipe(
        map(changes => {
          const data = changes.payload.data();
          const id = changes.payload.id;
          return { id, ...data };
        }))
  }

  createTopic(topic: Topic){
    this.topicCollection.add(topic)
    .then(docRef => {
      console.log("Document written with ID: ", docRef.id);
      this.router.navigate(['/forum', docRef.id]);
    });
    //redirect seite öffnen


  }
// update if new posts
  updateTopic(topic: Topic, id: string){
    const topicDocuments = this.firestore.doc<TopicId>('Topic/' + id);

    topicDocuments.update(topic);

  }

// delete topic and all contained posts
  deleteTopic(topicid: string){
    this.topicCollection.doc(topicid).delete();
  }

  createPost() {

  }

  updatePost(){

  }


}
