import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { firestore } from 'firebase';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Chat } from '../models/chat';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private chatCollection: AngularFirestoreCollection = this.firestore.collection('Chats');
  private userCollection: AngularFirestoreCollection = this.firestore.collection('User');


  constructor(public auth: AngularFireAuth, private firestore: AngularFirestore, public router: Router) { }


  createChat(chid: string, chname: string){
    const uid = this.auth.auth.currentUser.uid;
    //let members: string[] = [uid, chid];
    this.chatCollection.add({
      lastUpdate: new Date()
    }).then(docRef => {
      console.log("Document written with ID: ", docRef.id);
      const chatid = docRef.id;
      this.userCollection.doc(uid).update({
        chats: firebase.firestore.FieldValue.arrayUnion({id: chatid, member: chname})
      });
      console.log('update');
      this.userCollection.doc(chid).update({
        chats: firebase.firestore.FieldValue.arrayUnion({id: chatid, member: this.auth.auth.currentUser.displayName})
      });
      this.router.navigate(['/chat', chatid]);
      

  })
    // inser Chat w/o messages
    // insert chatid in uid und chid

  }
  

  sendMessage(chid: string, messages: Chat ){
    // send button beim chat = update chat
    this.chatCollection.doc(chid).update(messages);
  }


  getAllChats(){
    return this.firestore.collection('User').doc<User>(this.auth.auth.currentUser.uid).snapshotChanges().pipe(
      map(changes => {
        const data = changes.payload.data().chats;
        return data;
      }))
    // get user > chat
  }

  getChat(id: string): Observable<Chat>{
    // get chat by id
    return this.firestore.collection('Chats').doc(id).snapshotChanges().pipe(
      map(changes => {
        const data = changes.payload.data() as Chat;
        return data;
      }))
  }

}
